// NOTE: This definition forces GLM to use radians (not degrees) for ALL of its
// angle arguments. The documentation may not always reflect this fact.
// YOU SHOULD USE THIS IN ALL FILES YOU CREATE WHICH INCLUDE GLM
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "glew/glew.h"
#include <GL/glut.h>

#include <fstream>
#include <iostream>
#include <string>
#include <time.h>

#include "Geometry.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Cube.h"
#include "FileReader.h"
#include "Camera.h"
#include "Voxel.h"

static const float PI = 3.141592653589f;

// Vertex arrays needed for drawing
unsigned int vboPos;
unsigned int vboCol;
unsigned int vboNor;
unsigned int vboIdx;

// Attributes
unsigned int locationPos;
unsigned int locationCol;
unsigned int locationNor;

// Uniforms
unsigned int unifModel;
unsigned int unifModelInvTr;
unsigned int unifViewProj;
unsigned int unifLightPos;
unsigned int unifLightCol;
unsigned int unifEyePos;
unsigned int unifDiffuse;

glm::mat4 myViewMat;
glm::mat4 myProjMat;
glm::mat4 myViewProj;

// Needed to compile and link and use the shaders
unsigned int shaderProgram;

// Window dimensions, change if you want a bigger or smaller window
unsigned int windowWidth = 1280;
unsigned int windowHeight = 720;

// Animation/transformation stuff
clock_t old_time;
float rotation = 0.0f;

// TESTING GEOMETRY
Geometry* geometry2;

// Helper function to read shader source and put it in a char array
// thanks to Swiftless
std::string textFileRead(const char*);

// Some other helper functions from CIS 565 and CIS 277
void printLinkInfoLog(int);
void printShaderInfoLog(int);
void printGLErrorLog();

// Standard glut-based program functions
void init(void);
void resize(int, int);
void display(void);
void keypress(unsigned char, int, int);
void mousepress(int button, int state, int x, int y);
void cleanup(void);

void initShader();
void cleanupShader();

// Rope stuff
Rope* rope;
void traverseBuffer();

void uploadGeometry(Geometry* geometry);
void drawGeometry(Geometry* geometry, glm::mat4 model);

Node* currentNode;
int index;
vector<Node*> nodeList;
glm::vec3 tempColor;
bool flashFlag;
int step;

Camera* cam;

// FileReader
FileReader* fileReader;

// Light position and color (to be read in from input file)
glm::vec3 lightPosition;
glm::vec3 lightColor;
glm::vec3 eyePos;

int numSpheres;


static void printVector(glm::vec3 v) {
	cout << "(" << v[0] << ", " << v[1] << ", " << v[2] << ")" << endl;
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    // Use RGBA double buffered window
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(windowWidth, windowHeight);
    glutCreateWindow("Exploring Knots");

    glewInit();

	string fileName = "knot.txt";
	if (argc >= 2) {
		fileName = argv[1];
	}

	fileReader = new FileReader(fileName);
	if (!fileReader->read()) {
		return 1;
	}
	rope = fileReader->rope;

	cam = new Camera();
	cam->setE(fileReader->EYEP);
	cam->setU(fileReader->UVEC);
	cam->setC(fileReader->VDIR);
	
	lightPosition = glm::vec3(fileReader->LPOS[0], fileReader->LPOS[1], fileReader->LPOS[2]);
	lightColor    = glm::vec3(fileReader->LCOL[0], fileReader->LCOL[1], fileReader->LCOL[2]);
	eyePos        = glm::vec3(fileReader->EYEP[0], fileReader->EYEP[1], fileReader->EYEP[2]);
		
	numSpheres = 0;

	currentNode = rope->root;

	
    init();
    glutDisplayFunc(display);
    glutReshapeFunc(resize);
    glutKeyboardFunc(keypress);
    glutMouseFunc(mousepress);
    glutIdleFunc(display);

    glutMainLoop();
    return 0;
}

void traverse(Node* N, glm::mat4 T) {

	N->transform();
	T = T * N->transformation;

	N->totalTransformation = T;

	if (N->hasGeometry) {
		uploadGeometry(N->geometry);
		drawGeometry(N->geometry, T);
		numSpheres++;
	}

	Node* thisNode;
	for (unsigned int i = 0; i < N->children.size(); i++) {
		thisNode = N->children[i];
		traverse(thisNode, T);

	}

}

void init() {
    // Create the VBOs and vboIdx we'll be using to render images in OpenGL
    glGenBuffers(1, &vboPos);
    glGenBuffers(1, &vboCol);
    glGenBuffers(1, &vboNor);
    glGenBuffers(1, &vboIdx);


    // Set the color which clears the screen between frames
    glClearColor(135.0/255.0, 200.0/255.0, 255.0/255.0, 0.5);
    // Enable and clear the depth buffer
    glEnable(GL_DEPTH_TEST);
    glClearDepth(1.0);
    glDepthFunc(GL_LEQUAL);

    // Set up our shaders here
    initShader();

	tempColor = glm::vec3(1.0f);


	index = 0;
	flashFlag = false;
	step = 0;
	
    resize(windowWidth, windowHeight);
    old_time = clock();
}

void initShader() {
    // Read in the shader program source files
    std::string vertSourceS = textFileRead("shaders/diff.vert.glsl");
    const char *vertSource = vertSourceS.c_str();
    std::string fragSourceS = textFileRead("shaders/diff.frag.glsl");
    const char *fragSource = fragSourceS.c_str();

    // Tell the GPU to create new shaders and a shader program
    GLuint shadVert = glCreateShader(GL_VERTEX_SHADER);
    GLuint shadFrag = glCreateShader(GL_FRAGMENT_SHADER);
    shaderProgram = glCreateProgram();

    // Load and compiler each shader program
    // Then check to make sure the shaders complied correctly
    // - Vertex shader
    glShaderSource    (shadVert, 1, &vertSource, NULL);
    glCompileShader   (shadVert);
    printShaderInfoLog(shadVert);
    
	// - Diffuse fragment shader
    glShaderSource    (shadFrag, 1, &fragSource, NULL);
    glCompileShader   (shadFrag);
    printShaderInfoLog(shadFrag);

    // Link the shader programs together from compiled bits
    glAttachShader  (shaderProgram, shadVert);
    glAttachShader  (shaderProgram, shadFrag);
    glLinkProgram   (shaderProgram);
    printLinkInfoLog(shaderProgram);

    // Clean up the shaders now that they are linked
    glDetachShader(shaderProgram, shadVert);
    glDetachShader(shaderProgram, shadFrag);
    glDeleteShader(shadVert);
    glDeleteShader(shadFrag);

    // Find out what the GLSL locations are, since we can't pre-define these
    locationPos    = glGetAttribLocation (shaderProgram, "vs_Position");
    locationNor    = glGetAttribLocation (shaderProgram, "vs_Normal");
    locationCol    = glGetAttribLocation (shaderProgram, "vs_Color");
    unifViewProj   = glGetUniformLocation(shaderProgram, "u_ViewProj");
    unifModel      = glGetUniformLocation(shaderProgram, "u_Model");
    unifModelInvTr = glGetUniformLocation(shaderProgram, "u_ModelInvTr");

	unifLightPos   = glGetUniformLocation(shaderProgram, "u_lightPos");
	unifLightCol   = glGetUniformLocation(shaderProgram, "u_lightCol");
	unifEyePos     = glGetUniformLocation(shaderProgram, "u_eyePos");

    printGLErrorLog();
}

void cleanup() {
    glDeleteBuffers(1, &vboPos);
    glDeleteBuffers(1, &vboCol);
    glDeleteBuffers(1, &vboNor);
    glDeleteBuffers(1, &vboIdx);

    glDeleteProgram(shaderProgram);
	
    delete geometry2;
}

void keypress(unsigned char key, int x, int y) {
    switch (key) {
    case 'q':
        cleanup();
        exit(0);
        break;
	case 'n':
		index++;

		currentNode->color = tempColor;
		currentNode->refreshColors();

		currentNode = nodeList[index % nodeList.size()];
		tempColor = currentNode->color;
		currentNode->color = currentNode->color * 1.5f;
		currentNode->refreshColors();
		break;
	case 'N':
		index = 0;

		currentNode->color = tempColor;
		currentNode->refreshColors();

		currentNode = nodeList[0];
		tempColor = currentNode->color;
		currentNode->color = currentNode->color * 1.5f;
		currentNode->refreshColors();
		break;
	case 'a':
		currentNode->setXTrans(currentNode->transVec.x - 0.5);
		break;
    case 'd':
		currentNode->setXTrans(currentNode->transVec.x + 0.5);
		break;
	case 's':
		currentNode->setYTrans(currentNode->transVec.y - 0.5);
		break;
    case 'w':
		currentNode->setYTrans(currentNode->transVec.y + 0.5);
		break;
	case 'r':
		currentNode->setZTrans(currentNode->transVec.z - 0.5);
		break;
    case 'e':
		currentNode->setZTrans(currentNode->transVec.z + 0.5);
		break;
	// Scale
	case 'x':
		currentNode->setXScale(currentNode->scaleVec.x + 0.5);
		break;
	case 'X':
		currentNode->setXScale(currentNode->scaleVec.x - 0.5);
		break;
	case 'y':
		currentNode->setYScale(currentNode->scaleVec.y + 0.5);
		break;
	case 'Y':
		currentNode->setYScale(currentNode->scaleVec.y - 0.5);
		break;
	case 'z':
		currentNode->setZScale(currentNode->scaleVec.z + 0.5);
		break;
	case 'Z':
		currentNode->setZScale(currentNode->scaleVec.z - 0.5);
		break;
	// Rotation
	case 'j':
		currentNode->setXRotation(currentNode->rotVec.x + (10.0f * PI / 180.0f));
		break;
	case 'J':
		currentNode->setXRotation(currentNode->rotVec.x - (10.0f * PI / 180.0f));
		break;
	case 'k':
		currentNode->setYRotation(currentNode->rotVec.y + (10.0f * PI / 180.0f));
		break;
	case 'K':
		currentNode->setYRotation(currentNode->rotVec.y - (10.0f * PI / 180.0f));
		break;
	case 'l':
		currentNode->setZRotation(currentNode->rotVec.z + (10.0f * PI / 180.0f));
		break;
	case 'L':
		currentNode->setZRotation(currentNode->rotVec.z - (10.0f * PI / 180.0f));
		break;
	case 'f':
		lightPosition[0] = lightPosition[0] + 0.5;
		break;
	case 'F':
		lightPosition[0] = lightPosition[0] - 0.5;
		break;
	case 'g':
		lightPosition[1] = lightPosition[1] + 0.5;
		break;
	case 'G':
		lightPosition[1] = lightPosition[1] - 0.5;
		break;
	case 'h':
		lightPosition[2] = lightPosition[2] + 0.5;
		break;
	case 'H':
		lightPosition[2] = lightPosition[2] - 0.5;
		break;
	case ' ':
		rope->simulate();
		break;
	case '1' :
		rope->thinXY();
		break;
	case '2' :
		rope->thinXZ();
		break;
	case '3' :
		rope->thinYZ();
		break;
	//case '4' :
	//	rope->pushXY();
	//	break;
	//case '5' :
	//	rope->pushXZ();
	//	break;
	//case '6' :
	//	rope->pushYZ();
	//	break;
	}

    glutPostRedisplay();
}

void mousepress(int button, int state, int x, int y) {
}

void display() {
    // Clear the screen so that we only see newly drawn images
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    clock_t newTime = clock();
    rotation += 2.5f * (static_cast<float>(newTime - old_time) / static_cast<float>(CLOCKS_PER_SEC));
    old_time = newTime;

    // Create a matrix to pass to the model matrix uniform variable in the
    // vertex shader, which is used to transform the vertices in our draw call.
    // The default provided value is an identity matrix; you'll change this.
    glm::mat4 modelmat = glm::mat4();

	glUniform4f(unifLightPos, lightPosition[0], lightPosition[1], lightPosition[2], 1);
	glUniform4f(unifLightCol, lightColor[0], lightColor[1], lightColor[2], 1);
	glUniform4f(unifEyePos, eyePos[0], eyePos[1], eyePos[2], 1);

    // Make sure you're using the right program for rendering
    glUseProgram(shaderProgram);

	// Upload all our little spheres
	numSpheres = 0;
	traverse(currentNode, glm::mat4(1.0f));
	//cout << "We have " << numSpheres << " spheres" << endl;
	
    // Move the rendering we just made onto the screen
    glutSwapBuffers();

    // Check for any GL errors that have happened recently
    printGLErrorLog();
}

void uploadGeometry(Geometry* geometry) {

	int VERTICES = geometry->getVertexCount();
	int INDICES = geometry->getIndexCount();

    // Sizes of the various array elements below.
    static const GLsizei SIZE_POS = sizeof(glm::vec3);
    static const GLsizei SIZE_NOR = sizeof(glm::vec3);
    static const GLsizei SIZE_COL = sizeof(glm::vec3);
    static const GLsizei SIZE_IND = sizeof(GLuint);
	
	vector<glm::vec3> positions = geometry->getVertices();
	vector<glm::vec3> normals = geometry->getNormals();
	vector<unsigned int> indices = geometry->getIndices();
	vector<glm::vec3> colors = geometry->getColors();

    // ================UPLOADING CODE (GENERALLY, ONCE PER CHANGE IN DATA)==============
    // Now we put the data into the Vertex Buffer Object for the graphics system to use
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    // Use STATIC_DRAW since the square's vertices don't need to change while the program runs.
    // Take a look at STREAM_DRAW and DYNAMIC_DRAW to see when they should be used.
    // Always make sure you are telling OpenGL the right size to make the buffer. Here we need 16 floats.
    glBufferData(GL_ARRAY_BUFFER, VERTICES * SIZE_POS, &positions[0], GL_STATIC_DRAW);

    // Bind+upload the color data
    glBindBuffer(GL_ARRAY_BUFFER, vboCol);
    glBufferData(GL_ARRAY_BUFFER, VERTICES * SIZE_POS, &colors[0], GL_STATIC_DRAW);

    // Bind+upload the normals
    glBindBuffer(GL_ARRAY_BUFFER, vboNor);
    glBufferData(GL_ARRAY_BUFFER, VERTICES * SIZE_POS, &normals[0], GL_STATIC_DRAW);

    // Bind+upload the indices to the GL_ELEMENT_ARRAY_BUFFER.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIdx);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, INDICES * SIZE_IND, &indices[0], GL_STATIC_DRAW);

    // Once data is loaded onto the GPU, we are done with the float arrays.
    // For your scene graph implementation, you shouldn't create and delete the vertex information
    // every frame. You would probably want to store and reuse them.

}

void drawGeometry(Geometry* geometry, glm::mat4 model) {

	int INDICES = geometry->getIndexCount();

	// Tell the GPU which shader program to use to draw things
    glUseProgram(shaderProgram);

    // Take a close look at how vertex, normal, color, and index informations
    // are created and uploaded to the GPU for drawing. You will need to do
    // something similar to get your scene graph to draw.
	rotation = 0.0f;
    model = glm::rotate(model, rotation, glm::vec3(1, 1, 1));
	
    // =============================== Draw the data that we sent =================================
    // Activate our three kinds of vertex information
    glEnableVertexAttribArray(locationPos);
    glEnableVertexAttribArray(locationCol);
    glEnableVertexAttribArray(locationNor);

    // Set the 4x4 model transformation matrices
    // Pointer to the first element of the array
    glUniformMatrix4fv(unifModel, 1, GL_FALSE, &model[0][0]);

    // Also upload the inverse transpose for normal transformation
    const glm::mat4 modelInvTranspose = glm::inverse(glm::transpose(model));
    glUniformMatrix4fv(unifModelInvTr, 1, GL_FALSE, &modelInvTranspose[0][0]);

    // Tell the GPU where the positions are: in the position buffer (4 components each)
    glBindBuffer(GL_ARRAY_BUFFER, vboPos);
    glVertexAttribPointer(locationPos, 3, GL_FLOAT, false, 0, NULL);

    // Tell the GPU where the colors are: in the color buffer (4 components each)
    glBindBuffer(GL_ARRAY_BUFFER, vboCol);
    glVertexAttribPointer(locationCol, 3, GL_FLOAT, false, 0, NULL);

    // Tell the GPU where the normals are: in the normal buffer (4 components each)
    glBindBuffer(GL_ARRAY_BUFFER, vboNor);
    glVertexAttribPointer(locationNor, 3, GL_FLOAT, false, 0, NULL);

    // Tell the GPU where the indices are: in the index buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIdx);

    // Draw the elements. Here we are only drawing 2 triangles * 3 vertices per triangle, for a
    // total of 6 elements.
    glDrawElements(GL_TRIANGLES, INDICES, GL_UNSIGNED_INT, 0);

    // Shut off the information since we're done drawing.
    glDisableVertexAttribArray(locationPos);
    glDisableVertexAttribArray(locationCol);
    glDisableVertexAttribArray(locationNor);

    // Check for OpenGL errors
    printGLErrorLog();
}

void resize(int width, int height) {
    // Set viewport
    glViewport(0, 0, width, height);

    // Get camera information
    // Add code here if you want to play with camera settings/ make camera interactive.
    //glm::mat4 projection = glm::perspective(PI / 4, width / (float) height, 0.1f, 100.0f);
    //glm::mat4 camera = glm::lookAt(glm::vec3(0, 0, 10), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	glm::mat4 projection = glm::perspective(fileReader->FOVY, fileReader->RESOwidth / (float) fileReader->RESOheight, 0.1f, 100.0f);
	glm::mat4 camera = glm::lookAt(fileReader->EYEP, fileReader->VDIR, fileReader->UVEC);
    projection = projection * camera;

    // Upload the projection matrix, which changes only when the screen or
    // camera changes

    glUseProgram(shaderProgram);
    glUniformMatrix4fv(unifViewProj, 1, GL_FALSE, &projection[0][0]);

    glutPostRedisplay();
}

std::string textFileRead(const char *filename) {
    // http://insanecoding.blogspot.com/2011/11/how-to-read-in-file-in-c.html
    std::ifstream in(filename, std::ios::in);
    if (!in) {
        std::cerr << "Error reading file" << std::endl;
        throw (errno);
    }
    return std::string(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());
}

void printGLErrorLog() {
    GLenum error = glGetError();
    if (error != GL_NO_ERROR) {
        std::cerr << "OpenGL error " << error << ": ";
        const char *e =
            error == GL_INVALID_OPERATION             ? "GL_INVALID_OPERATION" :
            error == GL_INVALID_ENUM                  ? "GL_INVALID_ENUM" :
            error == GL_INVALID_VALUE                 ? "GL_INVALID_VALUE" :
            error == GL_INVALID_INDEX                 ? "GL_INVALID_INDEX" :
            "unknown";
        std::cerr << e << std::endl;

        // Throwing here allows us to use the debugger stack trace to track
        // down the error.
#ifndef __APPLE__
        // But don't do this on OS X. It might cause a premature crash.
        // http://lists.apple.com/archives/mac-opengl/2012/Jul/msg00038.html
        throw;
#endif
    }
}

void printLinkInfoLog(int prog) {
    GLint linked;
    glGetProgramiv(prog, GL_LINK_STATUS, &linked);
    if (linked == GL_TRUE) {
        return;
    }
    std::cerr << "GLSL LINK ERROR" << std::endl;

    int infoLogLen = 0;
    int charsWritten = 0;
    GLchar *infoLog;

    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &infoLogLen);

    if (infoLogLen > 0) {
        infoLog = new GLchar[infoLogLen];
        // error check for fail to allocate memory omitted
        glGetProgramInfoLog(prog, infoLogLen, &charsWritten, infoLog);
        std::cerr << "InfoLog:" << std::endl << infoLog << std::endl;
        delete[] infoLog;
    }
    // Throwing here allows us to use the debugger to track down the error.
    throw;
}

void printShaderInfoLog(int shader) {
    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled == GL_TRUE) {
        return;
    }
    std::cerr << "GLSL COMPILE ERROR" << std::endl;

    int infoLogLen = 0;
    int charsWritten = 0;
    GLchar *infoLog;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

    if (infoLogLen > 0) {
        infoLog = new GLchar[infoLogLen];
        // error check for fail to allocate memory omitted
        glGetShaderInfoLog(shader, infoLogLen, &charsWritten, infoLog);
        std::cerr << "InfoLog:" << std::endl << infoLog << std::endl;
        delete[] infoLog;
    }
    // Throwing here allows us to use the debugger to track down the error.
    throw;
}
